# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_clean = tplroot ~ '.service.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as gdm with context %}

include:
  - {{ sls_service_clean }}

{% if salt['pillar.get']('gdm-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_gdm', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

gdm-config-file-gdm-config-{{ name }}-absent:
  file.absent:
    - name: /var/lib/AccountsService/users/{{ name }}

gdm-config-file-gdm-face-{{ name }}-absent:
  file.absent:
    - name: /var/lib/AccountsService/icons/{{ name }}

{% endif %}
{% endfor %}
{% endif %}
