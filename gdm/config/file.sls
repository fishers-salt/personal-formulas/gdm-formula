# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as gdm with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- from tplroot ~ "/libusers.jinja" import get_user_value_or_default %}

include:
  - {{ sls_package_install }}

gdm-config-file-accounts-service-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService
    - user: root
    - group: root
    - mode: '0775'
    - makedirs: True

gdm-config-file-accounts-service-users-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/users
    - user: root
    - group: root
    - mode: '0700'
    - require:
      - gdm-config-file-accounts-service-dir-managed

gdm-config-file-accounts-service-icons-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/icons
    - user: root
    - group: root
    - mode: '0775'
    - require:
      - gdm-config-file-accounts-service-dir-managed

{% if salt['pillar.get']('gdm-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_gdm', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

gdm-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

{% set email = user.get('email', None) %}
{% set language = user.get('language', None) %}
{% set session = get_user_value_or_default(user, 'session') %}
{% set face = user.get('icon_src', None) %}
{% set face_hash = user.get('icon_src_hash', None) %}
gdm-config-file-gdm-config-{{ name }}-managed:
  file.managed:
    - name: /var/lib/AccountsService/users/{{ name }}
    - source: {{ files_switch([
                  name ~ '-gdm-user.tmpl',
                  'gdm-user.tmpl'],
                lookup='gdm-config-file-gdm-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0600'
    - user: root
    - group: {{ gdm.rootgroup }}
    - template: jinja
    - context:
        email: {{ email }}
        icon: {{ face }}
        language: {{ language }}
        name: {{ name }}
        session: {{ session }}
    - require:
      - gdm-config-file-accounts-service-users-dir-managed

{% if face != None %}
gdm-config-file-gdm-face-{{ name }}-managed:
  file.managed:
    - name: /var/lib/AccountsService/icons/{{ name }}
    - source: {{ face }}
    - source_hash: {{ face_hash }}
    - mode: '0644'
    - user: root
    - group: root
    - require:
      - gdm-config-file-accounts-service-icons-dir-managed
{% endif %}

{% endif %}
{% endfor %}
{% endif %}
