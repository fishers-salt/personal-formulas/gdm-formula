# frozen_string_literal: true

control 'gdm-service-running' do
  title 'should be running and enabled'

  describe service('gdm.service') do
    it { should be_enabled }
    it { should be_running }
  end
end
