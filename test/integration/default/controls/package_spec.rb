# frozen_string_literal: true

pkg =
  case os[:name]
  when 'arch', 'fedora'
    'gdm'
  else
    'gdm3'
  end

control 'gdm-package-install-pkg--installed' do
  title 'should be installed'

  describe package(pkg) do
    it { should be_installed }
  end
end
