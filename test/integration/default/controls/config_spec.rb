# frozen_string_literal: true

control 'gdm-config-file-accounts-service-dir-managed' do
  title 'should exist'

  describe directory('/var/lib/AccountsService') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0775' }
  end
end

control 'gdm-config-file-accounts-service-users-dir-managed' do
  title 'should exist'

  describe directory('/var/lib/AccountsService/users') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'gdm-config-file-accounts-service-icons-dir-managed' do
  title 'should exist'

  describe directory('/var/lib/AccountsService/icons') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0775' }
  end
end

control 'gdm-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'gdm-config-file-gdm-config-auser-managed' do
  title 'should match desired lines'

  describe file('/var/lib/AccountsService/users/auser') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('Session=gnome') }
    its('content') { should include('Email=jmiller@ceres.station') }
    its('content') { should include('Icon=/var/lib/AccountsService/icons/auser') }
  end
end

control 'gdm-config-file-gdm-face-auser-managed' do
  title 'should be present'

  describe file('/var/lib/AccountsService/icons/auser') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('sha256sum') do
      should eq 'ae977addba8c3f1c3215228672d619b366e3f293a3b768f0467fc4c5ec26b68a'
    end
  end
end
