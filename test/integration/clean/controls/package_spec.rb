# frozen_string_literal: true

pkg =
  case os[:name]
  when 'arch', 'fedora'
    'gdm'
  else
    'gdm3'
  end

control 'gdm-package-install-pkg-installed' do
  title 'should not be installed'

  describe package(pkg) do
    it { should_not be_installed }
  end
end
