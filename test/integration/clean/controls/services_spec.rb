# frozen_string_literal: true

control 'gdm-service-clean' do
  title 'should be stopped and disabled'

  describe service('gdm.service') do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end
