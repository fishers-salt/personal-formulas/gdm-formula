# frozen_string_literal: true

control 'gdm-config-file-gdm-config-auser-absent' do
  title 'should match desired lines'

  describe file('/var/lib/AccountsService/users/auser') do
    it { should_not exist }
  end
end

control 'gdm-config-file-gdm-face-auser-absent' do
  title 'should not be present'

  describe file('/var/lib/AccountsService/icons/auser') do
    it { should_not exist }
  end
end
